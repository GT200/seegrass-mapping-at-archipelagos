# Seegrass mapping Archipelagos
![Alt seegsrass image](./seegrass.jpg)

## Seagrass mapping project


## Description
Seagrasses are a group of plants composed of just four families that play an important role in
underwater ecology including water oxygenation, carbon storage, stabilizing the see bottom and reducing
the velocity of currents, producing organic matter, a food source, a spawning and refuge area for
many organisms. However, the degradation of the undersea prairies has accelerated in recent years
due to, among others, fishing pollution, ocean acidification, coastal activities and invasive plant and animal species (Traganos and Reinartz, 2018). Or Seagrass meadows is very sensitive due to its slow growth rate, vulnerability to anthropogenic impacts and difficulty to recover once destroyed. 
For targeted protection of these underwater habitats, low-cost, simple, reliable, accurate and timely
mapping is required and publish the map in international scientific databases, such as ’EMOD.net’
To map these underwater meadows, Archipelagos has developed several 
simple, low-cost techniques.These techniques include:
    ● SONAR mapping
    ● DRONE mapping
    ● SATELLITE mapping
    ● CITIZEN SCIENCE mapping
## Objectives
# [Objective 1]
To highlight the deficiency of the Official Seagras Distribution Map in Greece, Archipelagos has designed different approaches to seagrass mapping that can offer better accuracy to determinate seagrass presence. The selected study areas for the mapping are:
    ● Lipsi: Sonar and drone seagrass mapping.
    ● Fourni: Is an island with a complex coastline between the islands of Samos and Ikaria. 
This island is featured as 0% seagrass presence, but Archipelagos did some preliminary surveys in the area estimating a real seagrass coverage of ~70%.
# [Objective 2]
The primary objective of this project is to publish an open source GIS database to EMODnet (https://emodnet.eu/en ). The European Marine Observation and Data Network (EMODnet) is a network of organisations supported by the EU’s integrated maritime policy. These organisations work together to observe the sea, process the data according to international standards and make that information freely available as interoperable data layers and data products. EMODnet has a specific branch for SEABED HABITATS data that fit perfectly with our data
## SONAR mapping:
SONAR mapping with Kayak: kayak based mapping and the subsequent processing of sonnar data in ReefMaste and QGIS.

view exemple (at this stage): 
![Alt seegsrass image](./map.png)
## Drone Imaging
TODO

## SATELLITE mapping
TODO

## CITIZEN SCIENCE mapping
TODO

## Support

![Alt archipelagos logo](./archiCoord.png)
## Roadmap
TODO
If you have ideas for releases in the future, it is a good idea to list them in the README.

## Contributing
TODO
State if you are open to contributions and what your requirements are for accepting them.

For people who want to make changes to your project, it's helpful to have some documentation on how to get started. Perhaps there is a script that they should run or some environment variables that they need to set. Make these steps explicit. These instructions could also be useful to your future self.

You can also document commands to lint the code or run tests. These steps help to ensure high code quality and reduce the likelihood that the changes inadvertently break something. Having instructions for running tests is especially helpful if it requires external setup, such as starting a Selenium server for testing in a browser.

## Authors and acknowledgment
TODO
Show your appreciation to those who have contributed to the project.

## License
For open source projects, say how it is licensed.

## Project status
If you have run out of energy or time for your project, put a note at the top of the README saying that development has slowed down or stopped completely. Someone may choose to fork your project or volunteer to step in as a maintainer or owner, allowing your project to keep going. You can also make an explicit request for maintainers.

 
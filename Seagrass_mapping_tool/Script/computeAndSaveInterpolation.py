"""
Tool:               <Seegrass Interpolation from sonar data>
Source Name:        <main_interpolartion>
Version:            <2.9.3>
Author:             <Gaëtan TIAZARA>
Usage:              <this is an arcGIS toolbox. Just open the project toolbox>
Required Arguments: <parameter0: waypoint shapefile or table>
                    <parameter1: interpolation methode >
                    <parameter2: path to the output file   >
Description:        <interpolate output data from reefmaster >
"""


import InterpolationFonctionFinal
import arcpy
# from arcpy.sa import *
import numpy as np


def InterpolateData(parameter1, x_obs, y_obs, z_obs, x_grd, y_grd):
    """
    param parameter1: interpolation methode 
    type parameter1 : string
    param x_obs, y_obs, z_obs: observations
    type x_obs, y_obs: np.array dimension n * 1 (2D array)
    x_grd, y_grd: positions for which we want to interpolate a z_int value
    type x_grd, y_grd: np array dimension m*p (2D array)

    return: interpolated values at x_grd, y_grd
    type return: np array dimension m*p (2D array)
    """
    if (parameter1 == "linear"):
        intepolated_value = InterpolationFonctionFinal.linearInterpolation(x_obs, y_obs, z_obs, x_grd, y_grd)
    elif (parameter1 == "nearest neighbour"):
        intepolated_value = InterpolationFonctionFinal.NearestInterpolation(x_obs, y_obs, z_obs, x_grd, y_grd)
    elif (parameter1 == "Inverse of distances"):
        p = 1
        intepolated_value = InterpolationFonctionFinal.inverseofDistanceInterpolation(x_obs, y_obs, z_obs, x_grd, y_grd, p)
    elif (parameter1 == "cubic"):
        intepolated_value = InterpolationFonctionFinal.cubictInterpolation(x_obs, y_obs, z_obs, x_grd, y_grd)
    elif (parameter1 == "clough tocher"):
        intepolated_value = InterpolationFonctionFinal.cloughTocherInterpolation(x_obs, y_obs, z_obs, x_grd, y_grd)
    elif (parameter1 == "radial basis functions"):
        intepolated_value = InterpolationFonctionFinal.cloughTocherInterpolation(x_obs, y_obs, z_obs, x_grd, y_grd)
    else:
        ###  statistic Method
        # dist, delta = InterpolationFonctionFinal.nuee(x_obs, y_obs, z_obs)
        # (list_dist_moy, list_gamma) = InterpolationFonctionFinal.vario(dist, 5000, delta)
        arcpy.AddMessage("start computing kr algo")
        intepolated_value, new_gamma, sigma_int_kr = InterpolationFonctionFinal.kringingInterpolation(x_obs, y_obs, z_obs, x_grd, y_grd)
    return intepolated_value

def normalizer(z_int):
    maximun = np.max(z_int)
    minimum = np.min(z_int)
    norm = (z_int - minimum)/(maximun - minimum)
    return norm

def clipper(in_features, clip_features, out_feature_class): 
    # Run Clip
    arcpy.analysis.Clip(in_features, clip_features, out_feature_class)

def inteprolatedPointToRaster(PointShapefile, outputRasterP_NP, interpolateDepth,  outputRasterDepth = np.array([[]])): 
    """
    
    """
    cellsize = 0.00015
    list_field = ["P_NP", "Depth"]
    for field in list_field:
        if field ==  list_field[0]:
            arcpy.PointToRaster_conversion(PointShapefile, field, outputRasterP_NP, cellsize= cellsize )
        else:
            if interpolateDepth =="yes":
                arcpy.PointToRaster_conversion(PointShapefile, field, outputRasterDepth, cellsize= cellsize )

def rasterizeInterpolatedPoint(PointShapefile, outputRasterP_NP, outputRasterDepth , outputVArRasterP_NP, outputVArRasterDepth, interpolateDepth):
    """
    # Description: Interpolates a surface from points using kriging.
    # Requirements: Spatial Analyst Extension
    # Import system module
    """ 
    list_field = ["P_NP", "Depth"]
    for field in list_field:
        # Setting local variables
        inFeatures = PointShapefile
        cellSize = 0.8*10**(-5)

        # Set complex variables
        kModelOrdinary = arcpy.sa.KrigingModelOrdinary("EXPONENTIAL")
        # kRadius = RadiusFixed(20000, 1)
        # Execute Kriging
        if field ==  list_field[0]:
            outKriging = arcpy.sa.Kriging(inFeatures, field, kModelOrdinary, cellSize, outputVArRasterP_NP)
            # Save the output 
            outKriging.save(outputRasterP_NP)
        else:
            if interpolateDepth =="yes":
                arcpy.AddMessage("Depth")
                outKriging = arcpy.sa.Kriging(inFeatures, field, kModelOrdinary, cellSize, outputVArRasterDepth)
                # Save the output >>
                outKriging.save(outputRasterDepth)

def createConvexHull(inFeatures, outFeatureClass): 
    arcpy.MinimumBoundingGeometry_management(inFeatures, outFeatureClass , "CONVEX_HULL")

def deleteOutOfConvexHullValue(x_grd, y_grd, z_grd, valuetodelete = 9999):
    x_grd = x_grd.reshape((-1, 1))
    y_grd = y_grd.reshape((-1, 1))
    z_grd = z_grd.reshape((-1, 1))
    arcpy.AddMessage("DELETING")
    del_index =  np.where(z_grd == valuetodelete)[0]
    z_grd = np.delete(z_grd, del_index , axis=0)
    x_grd = np.delete(x_grd, del_index , axis=0)
    y_grd = np.delete(y_grd, del_index , axis=0) 
    return (x_grd,y_grd, z_grd)

def extractRasterByMask(inRaster, inMaskData, outRaster):
    """
    """
    
    # Set local variables
    extraction_area = "INSIDE"
    # analysis_extent = "elevation"

    # Execute ExtractByMask
    outExtractByMask = arcpy.sa.ExtractByMask(inRaster, inMaskData)
    # Save the output 
    outExtractByMask.save(outRaster)

def SaveDataAsShapefile(parameter0, parameter2,  x_grd, y_grd, P_NP_grd_int, interpolateDepth = "yes", Depth_grd_int = np.array([[]]), ):
    """
    param parameter0: waypoint shapefile or table 
    type parameter0 : string
    param parameter2: waypoint shapefile or table 
    type parameter2 : string
    param x_grd, y_grd: positions for which we want to interpolate a z_int value
    type x_grd, y_grd: np array dimension m*p (2D array)

    return: interpolated values at x_grd, y_grd
    type return: np array dimension m*p (2D array)
    """
    #arcpy.AddMessage(P_NP_grd_int)
    #prepare date for saving as shape file or grid
    x_grd = x_grd.reshape((-1, 1))
    y_grd = y_grd.reshape((-1, 1))
    P_NP_grd_int = P_NP_grd_int.reshape((-1, 1))
    if interpolateDepth =="yes":
        Depth_grd_int = Depth_grd_int.reshape((-1, 1))

        interpolatedatasArray = np.array(list(zip(np.squeeze(x_grd), np.squeeze(y_grd), np.squeeze(P_NP_grd_int), np.squeeze(Depth_grd_int) )), dtype = [("Lon", "float"),("Lat", "float"), ("P_NP", "float"), ("Depth", "float")]).reshape((-1, 1))
    else: 

        interpolatedatasArray = np.array(list(zip(np.squeeze(x_grd), np.squeeze(y_grd), np.squeeze(P_NP_grd_int) )), dtype = [("Lon", "float"),("Lat", "float"), ("P_NP", "float")]).reshape((-1, 1))

    #getting System coordiante system
    SR = arcpy.Describe(parameter0).spatialReference
    #saving the array as shapefile
    arcpy.da.NumPyArrayToFeatureClass(interpolatedatasArray, parameter2, ['Lon', 'Lat'], SR) 
    # arcpy.da.NumPyArrayToTable(interpolatedatasArray, "../SonarInterpolateMapSeegrass.gdb/out_table")


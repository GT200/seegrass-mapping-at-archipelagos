import arcpy

def AddAttributPNP(seagrass_grid_layer, seagrass_grid_Table, seagrass_raster_layer, raster_to_point_layer, useSegrassInterpolatedPoint, seagrassInterpolatedPoint,  in_symbology_layer, LowerLimitPosidonia):
    """
    """
    LowerLimitPosidonia = LowerLimitPosidonia.replace(",", ".")
    if useSegrassInterpolatedPoint =="no":
        arcpy.conversion.RasterToPoint(seagrass_raster_layer, raster_to_point_layer)
        pointSelected, Count = arcpy.management.SelectLayerByAttribute(in_layer_or_view=raster_to_point_layer, selection_type="NEW_SELECTION", where_clause="grid_code > " + str(LowerLimitPosidonia), invert_where_clause="")
    else:
        pointSelected, Count = arcpy.management.SelectLayerByAttribute(in_layer_or_view=seagrassInterpolatedPoint, selection_type="NEW_SELECTION", where_clause="P_NP > " + str(LowerLimitPosidonia), invert_where_clause="")
    try: 
        arcpy.da.TableToNumPyArray(seagrass_grid_layer, "P_NP")

        seagrass_grid_layer_selected, out_layers_or_views, Count = arcpy.management.SelectLayerByLocation(seagrass_grid_layer,"CONTAINS", pointSelected)
        arcpy.management.CalculateField(seagrass_grid_layer_selected, "P_NP", "1", "PYTHON3")

        if useSegrassInterpolatedPoint =="no":
            pointSelected, Count = arcpy.management.SelectLayerByAttribute(in_layer_or_view=raster_to_point_layer, selection_type="NEW_SELECTION", where_clause="grid_code < " + str(LowerLimitPosidonia), invert_where_clause="")
        else:
            pointSelected, Count = arcpy.management.SelectLayerByAttribute(in_layer_or_view=seagrassInterpolatedPoint, selection_type="NEW_SELECTION", where_clause="P_NP < " + str(LowerLimitPosidonia), invert_where_clause="")

        seagrass_grid_layer_selected, out_layers_or_views, Count = arcpy.management.SelectLayerByLocation(seagrass_grid_layer,"CONTAINS", pointSelected)
        arcpy.management.CalculateField(seagrass_grid_layer_selected, "P_NP", "0", "PYTHON3")
       
        symbologyFields = [["VALUE_FIELD", "P_NP", "P_NP"]]
        seagrass_grid_layer = arcpy.management.ApplySymbologyFromLayer(seagrass_grid_layer, in_symbology_layer, symbologyFields, update_symbology="UPDATE")[0]

    except:

        arcpy.management.AddField(seagrass_grid_Table, "P_NP", "FLOAT")
        arcpy.management.CalculateField(seagrass_grid_Table, "P_NP", "0", "PYTHON3")
        seagrass_grid_layer_selected, out_layers_or_views, Count = arcpy.management.SelectLayerByLocation(seagrass_grid_layer,"CONTAINS", pointSelected)
        arcpy.management.CalculateField(seagrass_grid_layer_selected, "P_NP", "1", "PYTHON3")
        pointSelected, Count = arcpy.management.SelectLayerByAttribute(raster_to_point_layer, "NEW_SELECTION")
        seagrass_grid_layer_selected, out_layers_or_views, Count  = arcpy.management.SelectLayerByLocation(seagrass_grid_layer,"CONTAINS", pointSelected, "", "NEW_SELECTION", "INVERT")
        arcpy.management.CalculateField(seagrass_grid_layer_selected, "P_NP", "-1", "PYTHON3")
        symbologyFields = [["VALUE_FIELD", "P_NP", "P_NP"]]
        seagrass_grid_layer = arcpy.management.ApplySymbologyFromLayer(seagrass_grid_layer, in_symbology_layer, symbologyFields, update_symbology="UPDATE")[0]
        # arcpy.SetParameterSymbology(1, in_symbology_layer)

    return (seagrass_grid_layer)


AddAttributPNP(arcpy.GetParameterAsText(0), arcpy.GetParameterAsText(1),  arcpy.GetParameterAsText(2), arcpy.GetParameterAsText(3) ,arcpy.GetParameterAsText(4), arcpy.GetParameterAsText(5), arcpy.GetParameterAsText(6), arcpy.GetParameterAsText(7))




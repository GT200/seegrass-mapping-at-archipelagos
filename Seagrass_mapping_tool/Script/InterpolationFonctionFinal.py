import numpy as np
import matplotlib.pyplot as plt
from scipy import interpolate
from scipy.interpolate import CloughTocher2DInterpolator
from scipy.interpolate.rbf import Rbf  # radial basis functions
import arcpy 

try:
    import skgstat as skg

    def kringingInterpolation(longitude, latitude, value, xx, yy):
        """
        longitude, latitude = coordianate (2-D array (n-line, p-column))
        value: observation variable to interpolate (2-D array (n-line, p-column))
        model: semi-variogram to use 
        n_lags = nb of class 
        """
        arcpy.AddMessage("ok, kriging interpolation skgstat fast")
        V = skg.Variogram(np.hstack((longitude, latitude)), value.reshape((-1, )),  estimator='matheron', n_lags=  100, model = 'exponential')
        ok = skg.OrdinaryKriging(V, min_points=5, max_points=15, mode='exact')

        inteporlated_field = ok.transform(xx.flatten(), yy.flatten()).reshape(xx.shape)
        sigma_error = ok.sigma.reshape(xx.shape)

        return(inteporlated_field, sigma_error)
except:
    print("the gstat library in not installed")

def linearInterpolation(longitude, latitude, value, xx, yy):
    """
    longitude, latitude = coordianate (2-D array (n-line, p-column))
    value: observation variable to interpolate (2-D array (n-line, p-column))
    model: semi-variogram to use 
    n_lags = nb of class 
    """
    arcpy.AddMessage("ok, linear interpolation scipy fast")
    inteporlated_field = interpolate.griddata(np.hstack((longitude, latitude)), value, (xx, yy), method='linear')

    return(inteporlated_field)

def NearestInterpolation(longitude, latitude, value, xx, yy):
    """
    longitude, latitude = coordianate (2-D array (n-line, p-column))
    value: observation variable to interpolate (2-D array (n-line, p-column))
    model: semi-variogram to use 
    n_lags = nb of class 
    """
    arcpy.AddMessage("ok, nearest interpolation scipy fast")

    inteporlated_field = interpolate.griddata(np.hstack((longitude, latitude)), value, (xx, yy), method='nearest')
    
    return(inteporlated_field)

def cubictInterpolation(longitude, latitude, value, xx, yy):
    """
    longitude, latitude = coordianate (2-D array (n-line, p-column))
    value: observation variable to interpolate (2-D array (n-line, p-column))
    model: semi-variogram to use 
    n_lags = nb of class 
    """
    arcpy.AddMessage("ok, cubic interpolation scipy fast")
    inteporlated_field = interpolate.griddata(np.hstack((longitude, latitude)), value, (xx, yy), method='cubic')
    
    return(inteporlated_field)

def cloughTocherInterpolation(longitude, latitude, value, xx, yy):
    interp = CloughTocher2DInterpolator(np.hstack((longitude, latitude)), value)
    inteporlated_field = interp(xx, yy)
    return(inteporlated_field)

def RBF(longitude, latitude, value, xx, yy):
    """
    longitude, latitude = coordianate (2-D array (n-line, p-column))
    value: observation variable to interpolate (2-D array (n-line, p-column))
    model: semi-variogram to use 
    n_lags = nb of class 
    """
    arcpy.AddMessage("ok, radial basis fonction interpolation scipy fast")
    rbf = Rbf(np.squeeze(longitude), np.squeeze(latitude), np.squeeze(value), function='gaussian')
    inteporlated_field = rbf(xx, yy)
    return(inteporlated_field)
    
def inverseofDistanceInterpolation(longitude, latitude, z_obs, xx, yy, p):
    """
    longitude, latitude = coordianate (2-D array (n-line, p-column))
    value: observation variable to interpolate (2-D array (n-line, p-column))
    model: semi-variogram to use 
    n_lags = nb of class 
    """
    arcpy.AddMessage("ok, inv interpolation")
    
    z_int_inv_dist =  np.nan*np.ones(xx.shape)
    
    for i in np.arange(0,xx.shape[0]):
        for j in np.arange(0,xx.shape[1]):
            val_x_grid = np.ones(longitude.shape) * xx[i,j]
            val_y_grid = np.ones(longitude.shape) * yy[i,j]
            
            dist = np.sqrt((val_x_grid-longitude)**2+(val_y_grid-latitude)**2)
            
            sum1 = np.sum(z_obs/(dist)**p)
            
            sum2 = np.sum(1/(dist)**p)
                
            z_int_inv_dist[i, j] =sum1/sum2
    return(z_int_inv_dist)

def SplineInterpolation(longitude, latitude, value, xx, yy, rho = 0):
    """
    descrtiption: Interpolation by spline
    param x_obs, y_obs, z_obs: observations (raw data) located at x_obs and y_ibs
    type x_obs, y_obs, z_obs: np.array dimension n*1, 2D array
    param x_int, y_int,positions for which we want to interpolate a z_int value
    type x_int, y_int: np array dimension m*p (2D array)
    param rho: weight of the inverse of distance 
    type rho:int
    return:  z_int, variable we want to interpolate
    type z_int: np array dimension m*p (2D array)
    """

    z_int_spl =  9999*np.ones(xx.shape)
    
    def phi(dist):
        return((dist**2)*(np.log(dist)))
    
    B = np.ones((longitude.shape[0] + 3, 1))
    B[:B.shape[0] - 3, 0:1] = value
    
    A = np.ones((longitude.shape[0] + 3, longitude.shape[0] + 3))         
    A[:A.shape[0] - 3, 0] = 1
    A[:A.shape[0] - 3, 1] = np.squeeze(longitude)
    A[:A.shape[0] - 3, 2]= np.squeeze(latitude)
    A[A.shape[0] - 3:, 3:] = A[: A.shape[0] - 3:, 0:3].T
    
    A_temp = A[:A.shape[0] - 3, 3:]
    for i in np.arange(0,A_temp.shape[0]):
        for j in np.arange(0,A_temp.shape[1]):
            
            if (i == j):
                A_temp[i,j] = rho
            else:
                si_x = longitude[i]
                si_y = latitude[i]
                sj_x = longitude[j]
                sj_y = latitude[j]
                dist = np.sqrt((si_x-sj_x)**2+(si_y-sj_y)**2)
                
                phi_dist = phi(dist)
                
                A_temp[i,j] = phi_dist
                
    A[:A.shape[0] - 3, 3:] = A_temp

    coeff = np.linalg.solve(A, B)
    
    for i in np.arange(0,xx.shape[0]):
        for j in np.arange(0,yy.shape[1]):
            
            x_s = xx[i, j]
            y_s = yy[i,j]
            
            dist_s_si = np.sqrt((longitude-x_s)**2+(latitude-y_s)**2)
            
            phi_dist_s_si = phi(dist_s_si)
            
            bi = coeff[3:, 0:1]
            sumphi = np.sum(bi * phi_dist_s_si)
            sum_ai = float(np.array([[1, x_s, y_s]])@coeff[:3, 0:1])
            z_int_spl[i, j] = sumphi + sum_ai
    
    return(z_int_spl)
"""
Tool:               <Seegrass Interpolation from sonar data>
Source Name:        <mainInterpolartion>
Version:            <2.9.3>
Author:             <Gaëtan TIAZARA>
Usage:              <this is an arcGIS toolbox. Just open the project toolbox>
Required Arguments: <waypointFeature: waypoint shapefile or table>
                    <interpolationMethod: interpolation methode >
                    <inteprolatedFeature: path to the output file   >
Description:        <interpolate out data from reefmaster >
"""
from computeAndSaveInterpolation import InterpolateData, SaveDataAsShapefile, rasterizeInterpolatedPoint, createConvexHull, extractRasterByMask, normalizer, deleteOutOfConvexHullValue, clipper, inteprolatedPointToRaster
import arcpy
import numpy as np

if __name__ == '__main__':
    # ScriptTool parameters
    # waypointFeature = "../OutputData/New Folder/islet2.shp"
    waypointFeature = arcpy.GetParameterAsText(0) # waypoint shapefile or table
    interpolationMethod = arcpy.GetParameterAsText(1) # interpolation methode
    inteprolatedFeature = arcpy.GetParameterAsText(2) # output file path
    clippingFeature = arcpy.GetParameterAsText(3)
    clippedFeature = arcpy.GetParameterAsText(4)
    rasterisedPNP = arcpy.GetParameterAsText(5) # outputRasterP_NP (Point to raster using kringing interpolation)
    rasterisedPNPError = arcpy.GetParameterAsText(6) # outputRasterDepth(Point to raster using kringing interpolation)
    rasterisedDepth = arcpy.GetParameterAsText(7) # Optional output P_NP raster where each cell contains the predicted variance values for that location.
    rasterisedDepthError = arcpy.GetParameterAsText(8) # Optional output Depth raster where each cell contains the predicted variance values for that location.
    ConvexHulloutfeature = arcpy.GetParameterAsText(9) # Minimum bounding geometry convex hull output
    PNPExtraxctedByMask = arcpy.GetParameterAsText(10) # Extract by mask P_NP_raster
    DepthExtraxctedByMask = arcpy.GetParameterAsText(11) # Extract by mask Depth_raster
    interpolateDepth = arcpy.GetParameterAsText(12)
    useKriking = arcpy.GetParameterAsText(13)
    grid_shape = int(arcpy.GetParameterAsText(14))
    UseConvexHullToClip = arcpy.GetParameterAsText(15)
    try:
        arcpy.CalculateGeometryAttributes_management(waypointFeature, [["X", "POINT_X"], ["Y", "POINT_Y"]])
    except:
        arcpy.AddMessage("exist")

    arcpy.AddMessage("Méthode:" + interpolationMethod)
    try:
        data = arcpy.da.TableToNumPyArray(waypointFeature, ("Longitude", "Latitude", "P_NP", "Depth"))
    except: 
        data = arcpy.da.TableToNumPyArray(waypointFeature, ("X", "Y", "P_NP", "Depth"))
    # Rearranging data
    list_data = []
    for tuples in data:
        items = []
        for item in tuples:
            items.append(item)
        list_data.append(items)
    array_data = np.array(list_data)
    # GPS coordiante
    x_obs = array_data[:,0:1].astype("float")
    y_obs = array_data[:,1:2].astype("float")
    # waypoint values
    P_NP_obs = array_data[:,2:3].astype("float")
    Depth_obs = array_data[:,3:4].astype("float")
    #arcpy.AddMessage(z_obs)
    # Creating a regular grid for interpolation
    # grid_shape = 1000
    arcpy.AddMessage("Grid: " + str(grid_shape))
    x_grd, y_grd = np.meshgrid(np.linspace(np.min(x_obs),np.max(x_obs), grid_shape), np.linspace(np.min(y_obs), np.max(y_obs), grid_shape))
    # interpolation of posidonia presence within the grid
    P_NP_grd_int = InterpolateData(interpolationMethod, x_obs, y_obs, P_NP_obs, x_grd, y_grd)
    # arcpy.AddMessage(np.where(P_NP_grd_int == 9999))
    P_NP_grd_int = np.nan_to_num(P_NP_grd_int, nan = 9999)
    # x_grd, y_grd, P_NP_grd_int = deleteOutOfConvexHullValue(x_grd, y_grd, P_NP_grd_int, valuetodelete = 9999)

    arcpy.AddMessage(np.unique(P_NP_grd_int))

    P_NP_grd_int = normalizer(P_NP_grd_int)

    
    if (interpolateDepth == "yes"):
        # interpolation of depth within the grid
        Depth_grd_int = InterpolateData("nearest neighbour", x_obs, y_obs, Depth_obs, x_grd, y_grd)
        Depth_grd_int = Depth_grd_int.reshape((-1, 1))
        Depth_grd_int = np.nan_to_num(Depth_grd_int, nan = 9999)
        # x_grd, y_grd, Depth_grd_int = deleteOutOfConvexHullValue(x_grd, y_grd, Depth_grd_int, valuetodelete = 9999)
        # arcpy.AddMessage(P_NP_grd_int)
        # save data as shapefile
        SaveDataAsShapefile(waypointFeature, inteprolatedFeature, x_grd, y_grd, P_NP_grd_int, interpolateDepth, Depth_grd_int)
        #
        if UseConvexHullToClip == "yes":
            createConvexHull(waypointFeature, ConvexHulloutfeature)
            clipper(inteprolatedFeature, ConvexHulloutfeature, clippedFeature)

        elif UseConvexHullToClip == "no":
            clipper(inteprolatedFeature, clippingFeature, clippedFeature)

        if useKriking == "yes": 
            rasterizeInterpolatedPoint(clippedFeature, rasterisedPNP, rasterisedDepth , rasterisedPNPError, rasterisedDepthError, interpolateDepth)
        else:
            # #
            inteprolatedPointToRaster(clippedFeature, rasterisedPNP, interpolateDepth,  rasterisedDepth)
            
        # #
        if UseConvexHullToClip == "yes":
            extractRasterByMask(rasterisedPNP, ConvexHulloutfeature, PNPExtraxctedByMask)
            # #
            extractRasterByMask(rasterisedDepth, ConvexHulloutfeature, DepthExtraxctedByMask)
        # #
        elif UseConvexHullToClip == "no":
            extractRasterByMask(rasterisedPNP, clippingFeature, PNPExtraxctedByMask)
            # #
            extractRasterByMask(rasterisedDepth, clippingFeature, DepthExtraxctedByMask)
    elif (interpolateDepth == "no"):
        SaveDataAsShapefile(waypointFeature, inteprolatedFeature, x_grd, y_grd, P_NP_grd_int, interpolateDepth = "no")
        #
        if UseConvexHullToClip == "yes":
            createConvexHull(waypointFeature, ConvexHulloutfeature)
            clipper(inteprolatedFeature, ConvexHulloutfeature, clippedFeature)

        elif UseConvexHullToClip == "no":
            clipper(inteprolatedFeature, clippingFeature, clippedFeature)
        # #
        if useKriking == "yes": 
            rasterizeInterpolatedPoint(clippedFeature, rasterisedPNP, rasterisedDepth , rasterisedPNPError, rasterisedDepthError, interpolateDepth)
        else:
            # #
            inteprolatedPointToRaster(clippedFeature, rasterisedPNP, interpolateDepth)
        
        if UseConvexHullToClip == "yes":
            extractRasterByMask(rasterisedPNP, ConvexHulloutfeature, PNPExtraxctedByMask)
            # #
            extractRasterByMask(rasterisedDepth, ConvexHulloutfeature, DepthExtraxctedByMask)
        # #
        elif UseConvexHullToClip == "no":
            extractRasterByMask(rasterisedPNP, clippingFeature, PNPExtraxctedByMask)
            # #
            extractRasterByMask(rasterisedDepth, clippingFeature, DepthExtraxctedByMask)
           





